import numpy as np
import numpy.linalg as LA
from numpy.linalg import eig

A = np.array([[11,-18],[6,-10]])
B = np.array([[-1],[-1],[1]])

MatriceIdentite = np.eye(3)
InverseMatrice = LA.inv(A)
Vecteur = np.array([[1],[2],[3]])
D, V = eig(A)


#print(D) # valeur propre
#print(V) # vecteur propre
#print(MatriceIdentite)
#print(np.diag(B))
#print("Addition",A+B)
#print("Produit", A@B)
#print("Transposée : ", A.T)
#print(InverseMatrice)
print(Vecteur @ B)