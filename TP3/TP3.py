import numpy as np
import numpy.linalg as LA
from matplotlib.pyplot import subplots
import matplotlib.pyplot as plt
import math

u1 = np.array(([1], [0]))
u2 = np.array(([0], [1]))

v1 = np.array(([1], [1]))
v2 = np.array(([1], [-1]))

print(u1[1])
def passage(v1, v2):
    v1_row = np.transpose(v1)
    v2_row = np.transpose(v2)
    P = np.concatenate((v1_row, v2_row), axis=0)
    return P

print(passage(v1,v2))

# Partie 2.2 question 3

fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True, sharex=True)
ax1.set_title('Ancienne base')
ax2.set_title('Nouvelle base')
plt.axis([-4, 4, -4, 4])

# Partie 2.2 question 4

ax1.plot([0,3],[0,5])
ax2.plot([0,3],[0,5])

# Partie 2.2 question 5

ax1.plot([0,u1[0,0]],[0,u1[1,0]])
ax1.plot([0,u2[0,0]],[0,u2[1,0]])
ax1.plot([0,v1[0,0]],[0,v1[1,0]])
ax1.plot([0,v2[0,0]],[0,v2[1,0]])

newu1 = LA.inv(passage(v1,v2)) @ u1
newu2 = LA.inv(passage(v1,v2)) @ u2
newv1 = LA.inv(passage(v1,v2)) @ v1
newv2 = LA.inv(passage(v1,v2)) @ v2

ax2.plot([0,newu1[0,0]],[0,newu1[1,0]])
ax2.plot([0,newu2[0,0]],[0,newu2[1,0]])
ax2.plot([0,newv1[0,0]],[0,newv1[1,0]])
ax2.plot([0,newv2[0,0]],[0,newv2[1,0]])

# Partie 2.3 première figure

#Construction rectangle

ax1.plot([-1,1,1,-1,-1],[-4,-4,1,1,-4])
ax2.plot(passage([-1,1,1,-1,-1],[-4,-4,1,1,-4]))

# Construction du cercle
#n = 2
#t = np.linspace(0,2*math.pi,n)
#C = np.array([np.cos(t),np.sin(t)])
#ax1.plot(C[0,:],C[1,:])
#ax2.plot(C[0,:],C[1,:])

plt.show()